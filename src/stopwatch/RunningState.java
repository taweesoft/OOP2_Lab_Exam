package stopwatch;

/**
 * Running state of stopwatch.
 * @author TAWEERAT CHAIMAN 5710546259.
 * @version 17.05.2015.
 */
public class RunningState extends State{
	
	/**
	 * @see State.start_btn(...)
	 * Stop the stopwatch and change state.
	 */
	@Override
	void start_btn(StopWatch stopwatch) {
		stopwatch.stop();
		stopwatch.setState(StopWatch.STOP_STATE);
	}
	
	/**
	 * @see State.reset_btn(...)
	 * Do nothing.
	 */
	@Override
	void reset_btn(StopWatch stopwatch) {
		/*Do nothing.*/
	}
	
	/**
	 * @see State.getElapsed(...)
	 * return the different time between start and current time.
	 */
	@Override
	long getElapsed(StopWatch stopwatch) {
		return stopwatch.getElapsedTime() + System.currentTimeMillis() - stopwatch.getStartTime();
	}
}
