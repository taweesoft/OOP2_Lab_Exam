package stopwatch;

import java.util.Observable;

/**
 * A StopWatch records elapsed time between each time
 * a "start" command and "stop" command are received.
 * It can be started and stopped many times, and the total
 * elapsed time is recorded.
 * To reset the elapsed time, call "reset" when the stopwatch
 * is stopped (you can't reset while the stopwatch is running).
 * 
 * Elapsed time is computed and returned in milliseconds.
 */

public class StopWatch extends Observable{
	/**Current state of stopwatch.*/
	private State state;
	/**All states of stopwatch.*/
	public static final State RUNNING_STATE = new RunningState();
	public static final State STOP_STATE = new StopState();
	/** the time the stopwatch was last started */
	private long startTime;
	/** the total elapsed time between starts and stops.  Invoke reset() to set to zero. */
	private long elapsed;
	
	/** Constructor for a new stopwatch. */
	public StopWatch(){
		/*Set first state is STOP STATE*/
		state = STOP_STATE;
		elapsed = 0;
	}
	
	/**Start the stopwatch by set starttime as now. */
	public void start() { 
		startTime = System.currentTimeMillis();
		setChanged();
		notifyObservers("Stop"); // Change the button label.
	}
	
	/**Add different time between now and starttime into elapsed */
	public void stop() {
		elapsed += System.currentTimeMillis() - startTime;
		setChanged();
		notifyObservers("Start"); //Change the button label.
	}
	
	/** 
	 * Return the elapsed time depend on state.
	 */
	public long getElapsed() {
		return state.getElapsed(this);
	}
	
	/** 
	 * Reset the stopwatch elapsed time to zero.
	 * Only works if stopwatch is stopped.
	 */
	public void reset() { 
		elapsed = 0;
	}
	
	/**
	 * Set state of stopwatch.
	 */
	public void setState(State state){
		this.state = state;
	}
	
	/**
	 * Get current state.
	 * @return State as current state.
	 */
	public State getState(){
		return state;
	}
	
	/**
	 * Get start time.
	 * @return long as start time.
	 */
	public long getStartTime(){
		return startTime;
	}
	
	/**
	 * Get current elapsed time (attribute).
	 * @return long as elapsed time.
	 */
	public long getElapsedTime(){
		return elapsed;
	}
}
