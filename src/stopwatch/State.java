package stopwatch;

/**
 * <<Interface>> State of stopwatch.
 * @author TAWEERAT CHAIMAN 5710546259.
 * @version 17.05.2015.
 */
public abstract class State {
	/**Performed when Start/Stop button has clicked.*/
	abstract void start_btn(StopWatch stopwatch);
	
	/**Performed when Reset button has clicked.*/
	abstract void reset_btn(StopWatch stopwatch);
	
	/**Get elapsed time of each state.*/
	abstract long getElapsed(StopWatch stopwatch);
}
