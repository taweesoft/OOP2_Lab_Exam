package stopwatch;

import javax.swing.SwingUtilities;

/**
 * Main application class.
 * @author TAWEERAT CHAIMAN 5710546259.
 * @vesion 17.05.2015.
 */
public class Main {
	
	/**
	 * Main method.
	 * @param args
	 */
	public static void main(String[] args) {
		/*Runnable to run with SwingUtilities*/
		Runnable runnable = new Runnable(){

			@Override
			public void run() {
				/*Create stopwatch.*/
				StopWatch stopwatch = new StopWatch();
				/*Create UI and inject stopwatch into ui via the constructor.*/
				StopWatchUI ui = new StopWatchUI(stopwatch);
				/*Run the ui.*/
				ui.run();
			}
			
		};
		
		/*Run the runnable to show the ui in other thread. (not main thread).*/
		SwingUtilities.invokeLater(runnable);
	}
}
