package stopwatch;

/**
 * Stopped state.
 * @author TAWEERAT CHAIMAN 5710546259.
 * @version 17.05.2015.
 */
public class StopState extends State{
	
	/**
	 * @see State.start_btn(...);
	 * Start the stopwatch and change state.
	 */
	@Override
	void start_btn(StopWatch stopwatch) {
		/*Start the stopwatch.*/
		stopwatch.start();
		/*Change state to running state.*/
		stopwatch.setState(StopWatch.RUNNING_STATE);
	}
	
	/**
	 * @see State.reset_btn(...);
	 * Reset the elapsed time of stopwatch.
	 */
	@Override
	void reset_btn(StopWatch stopwatch) {
		/*Reset the stopwatch time*/
		stopwatch.reset();
	}
	
	/**
	 * Get elapsed time in this state.
	 * The elapsed time should steady.
	 */
	@Override
	long getElapsed(StopWatch stopwatch) {
		/*Get elapsed time (Attribute)*/
		return stopwatch.getElapsedTime();
	}
}
