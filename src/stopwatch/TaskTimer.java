package stopwatch;

import java.util.TimerTask;

/**
 * Timer task for run the update display of ui.
 * @author TAWEERAT CHAIMAN 5710546259.
 * @version 17.05.2015.
 */
public class TaskTimer extends TimerTask{
	/**Attribute.*/
	private StopWatchUI ui;
	
	/**
	 * Constructor, receive StopWatchUI.
	 * @param ui
	 */
	public TaskTimer (StopWatchUI ui){
		/*Setting attribute.*/
		this.ui = ui;
	}
	
	/**
	 * The method that run event x millisecond. (x depend on timer).
	 */
	@Override
	public void run() {
		/*Update the LED display.*/
		ui.updateDisplay();
	}
}
