package stopwatch;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * User interface class.
 * @author TAWEERAT CHAIMAN 5710546259
 * @version 17.05.2015.
 */
public class StopWatchUI implements Observer{
	/**JComponents.*/
	private JFrame frame;
	private JLabel display;
	private JButton btn_run;
	/**Attribute.*/
	private StopWatch stopwatch;
	
	/**
	 * Constructor that injected the stopwatch.
	 * @param stopwatch.
	 */
	public StopWatchUI(StopWatch stopwatch){
		this.stopwatch = stopwatch;
		
		/*Add this view as observer of stopwatch.*/
		/*This let stopwatch know UI as Observer not StopWatchUI.*/
		stopwatch.addObserver(this);
		
		/*Initialize components.*/
		initComponents();
		
		/*Start timer.*/
		Timer timer = new Timer();
		TaskTimer task = new TaskTimer(this);
		timer.scheduleAtFixedRate(task, 1, 1);
	}

	/**
	 * Initialize view of GUI.
	 */
	public void initComponents(){
		/*Initialize JFrame.*/
		frame = new JFrame("StopWatch");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/*Initialize main panel*/
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(2,1));
		
		/*Set display view.*/
		display = new JLabel();
		display.setHorizontalAlignment(SwingConstants.RIGHT);
		display.setOpaque(true);
		display.setBackground(Color.BLACK);
		display.setForeground(Color.YELLOW);
		display.setFont(new Font("Arial",1,30));
		
		/*Set action to JButton by constructor injection.*/
		btn_run = new JButton(new RunButtonAction("Start"));
		JButton btn_reset = new JButton(new ResetButtonAction("Reset"));
		
		/*Set panel of buttons.*/
		JPanel btn_pane = new JPanel();
		btn_pane.add(btn_run);
		btn_pane.add(btn_reset);
		
		/*Add each component to main panel*/
		panel.add(display);
		panel.add(btn_pane);
		
		/*Add main panel to frame.*/
		frame.add(panel);
		frame.pack();
	}
	
	/**
	 * Show GUI.
	 */
	public void run() {
		frame.setVisible(true);
	}
	
	/**
	 * Start/Stop action. Action is depend on state of stopwatch.
	 */
	class RunButtonAction extends AbstractAction{
		
		/**
		 * Constructor that receive name(label) on component.
		 * @param name
		 */
		public RunButtonAction(String name){
			/*Set name as label on component.*/
			super(name);
		}
		
		/**
		 * Execute current state of stopwatch.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			/*Perform the state.*/
			stopwatch.getState().start_btn(stopwatch);
		}
	}
	
	/**
	 * Necessary method for Observer.
	 * Receive information from observable.
	 * @param o as Observable.
	 * @param arg as information send by observable.
	 */
	@Override
	public void update(Observable o, Object arg) {
		/*This if statement is necessary for check the object.
		 * I Hope it does not counted.*/
		if(arg == null) return;
		if(arg.getClass() != String.class) return;
		
		/*Update the button label.*/
		String msg = (String)arg;
		btn_run.setText(msg);
	}
	
	/**
	 * Reset button's action. Action depend on state of stopwatch.
	 */
	class ResetButtonAction extends AbstractAction{
		
		/**
		 * Constructor, receive name(label) on button.
		 * @param name
		 */
		public ResetButtonAction(String name){
			super(name);
		}
		
		/**
		 * Perform statement in current state of stopwatch.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			stopwatch.getState().reset_btn(stopwatch);
		}
	}
	
	/**
	 * Update the display every 1 millisecond.
	 */
	public void updateDisplay(){
		/*Get elapsed from stopwatch as millisecond and convert to second.*/
		double time = stopwatch.getElapsed()/1000.0;
		
		/*Set text on display.*/
		display.setText(String.format("%.3f", time));
	}
}
