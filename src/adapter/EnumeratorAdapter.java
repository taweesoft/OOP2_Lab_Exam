package adapter;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * Adapter to convert Enumeration to Iterator.
 * @author TAWEERAT CHAIMAN 5710546259.
 * @param <T>
 */
public class EnumeratorAdapter<T> implements Iterator<T>{
	/**Attribute.*/
	private Enumeration<T> enumeration;
	
	/**
	 * Constructor, receive Enumeration.
	 * @param e
	 */
	public EnumeratorAdapter(Enumeration<T> e){
		/*Setting attribute.*/
		this.enumeration = e;
	}

	/**
	 * Check for next element.
	 */
	@Override
	public boolean hasNext() {
		return enumeration.hasMoreElements();
	}
	
	/**
	 * Return next element.
	 */
	@Override
	public T next() {
		return enumeration.nextElement();
	}
	
}
